## CreditCardEntry
## Introduction
This library provides an elegant form for credit card entry that can be easily added to a ability or fraction.
Regex is used to validate credit card types and a Luhn check is performed on the card numbers. This form was inspired by the credit entry UI on Square.

 - Smooth interface
 - Identifies credit card type
 - Hides number for privacy
 - Supports VISA, MasterCard, Discover and AMEX

## Usage instructions
Please see below for an example.
XML
```
    <com.devmarvel.creditcardentry.library.CreditCardForm
        ohos:id="$+id:form_no_zip"
        ohos:width="match_content"
        ohos:height="match_content"
        ohos:padding="15vp"
        ohos:text_size="16fp"
        ohos:card_number_hint="XXXX XXXX XXXX XXXX"
        ohos:helper_text_color="$color:text_helper_color"
        ohos:include_zip="false"/>
```
 * `ohos:text_color` - change the input field's text color (`Color.BLACK` by default).
 * `ohos:hint_text_color` - change the input field's hint text color (`Color.LTGRAY` by default).
 * `ohos:cursor_color` - change the input field's cursor color (`Color.BLACK` by default).
 * `ohos:default_text_colors` - If true, use text colors provided by the app's theme instead of the
   values provided by `ohos:text_color`,`ohos:hint_text_color`, and `ohos:cursor_color`. This overrides
   the values for those three text colors and causes the text inputs to use the colors provided by
   the application's theme.
 * `ohos:include_helper` - boolean to show/hide the helper text under the widget (`true` by default (i.e. helper is shown))
 * `ohos:helper_text_color` - change the text color of the hints that appear below the widget by default.
 * `ohos:include_zip` - boolean to show/hide the zip code in the form (`true` by default (i.e. zip is shown))
 * `ohos:include_exp` - boolean to show/hide the exp in the form (`true` by default (i.e. exp is shown))
 * `ohos:include_security` - boolean to show/hide the security code in the form (`true` by default (i.e. security is shown))
 * `ohos:card_number_hint` - string to put in as a placeholder (hint) in the credit card number field
 * `ohos:input_background` - the drawable to use as a background (defaults to white square with black 1px border)

In code:
```
    public class MainAbility extends Ability {
  
      private DirectionalLayout directionalLayout;
      private CreditCardForm form;
  
      @Override
      public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        
        directionalLayout = (DirectionalLayout) findComponentById(R.id.layer);
		
        form = new CreditCardForm(this);
    	directionalLayout.addComponent(form);
    	
    	buttonAuthorize = (Button) findComponentById(ResourceTable.Id_buttonAuthorize);
    	buttonAuthorize.setClickedListener(new Component.ClickedListener() {
    		
    		@Override
    		public void onClick(Component arg0) {
    			if(form.isCreditCardValid())
    			{
    				CreditCard card = form.getCreditCard();
    				//Pass credit card to service
    			}
    			else
    			{
    				//Alert Credit card invalid
    			}
    		}
    	});
    }
```

## Installation instruction
**Method 1:**
Generate the .har package through the library and add the .har package to the libs folder.
Add the following code to the entry gradle:
```
implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
```
**Method 2:**
In project level build.gradle:
```
allprojects{
    repositories{
        mavenCentral()
    }
}
```
Add the following code to the entry gradle:
```
implementation project(path: ': creditCardEntry)
```
 