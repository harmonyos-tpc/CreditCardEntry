package com.devmarvel.creditcardentry.internal;

import com.devmarvel.creditcardentry.fields.CreditEntryFieldBase;
import com.devmarvel.creditcardentry.library.CardType;
import ohos.agp.components.TextField;

/**
 * contract for delegate
 * <p>
 * TODO gut this delegate business
 */
public interface CreditCardFieldDelegate {
    // When the card type is identified
    void onCardTypeChange(CardType type);

    void onCreditCardNumberValid(String remainder);

    void onExpirationDateValid(String remainder);

    // Image should flip to back for security code
    void onSecurityCodeValid(String remainder);

    void onZipCodeValid();

    void onBadInput(TextField field);

    void focusOnField(CreditEntryFieldBase field, String initialValue);

    void focusOnPreviousField(CreditEntryFieldBase field);
}
