/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.devmarvel.creditcardentry.util;

import ohos.app.Context;

/**
 * vp px fp conversion tool
 */
public class DisplayUtils {
    /**
     * Convert px to its equivalent vp
     *
     * @param context context
     * @param pxValue px value
     * @return vp value
     */
    public static int px2vp(Context context, float pxValue) {
        final float scale =
                context.getResourceManager().getDeviceCapability().screenDensity / 160f;
        return (int) (pxValue / scale + 0.5f);
    }


    /**
     * Convert vp to its equivalent px
     *
     * @param context context
     * @param vpValue vp value
     * @return px value
     */
    public static int vp2px(Context context, float vpValue) {
        final float scale =
                context.getResourceManager().getDeviceCapability().screenDensity / 160f;
        return (int) (vpValue * scale + 0.5f);
    }


    /**
     * Convert px to its equivalent fp
     *
     * @param context context
     * @param pxValue px value
     * @return fp value
     */
    public static int px2fp(Context context, float pxValue) {
        final float fontScale =
                context.getResourceManager().getDeviceCapability().screenDensity / 160f;
        return (int) (pxValue / fontScale + 0.5f);
    }


    /**
     * Convert fp to its equivalent px
     *
     * @param context context
     * @param fpValue fp value
     * @return px value
     */
    public static int fp2px(Context context, float fpValue) {
        final float fontScale =
                context.getResourceManager().getDeviceCapability().screenDensity / 160f;
        return (int) (fpValue * fontScale + 0.5f);
    }
}