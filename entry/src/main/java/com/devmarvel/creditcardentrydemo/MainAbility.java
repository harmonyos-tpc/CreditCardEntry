/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.devmarvel.creditcardentrydemo;

import com.devmarvel.creditcardentry.library.CardValidCallback;
import com.devmarvel.creditcardentry.library.CreditCard;
import com.devmarvel.creditcardentry.library.CreditCardForm;
import com.devmarvel.creditcardentry.util.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ScrollView;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbility extends Ability {
    private static final String TAG = "MainAbility";
    private ScrollView scrollView;

    CardValidCallback cardValidCallback = new CardValidCallback() {
        @Override
        public void cardValid(CreditCard card) {
            LogUtil.debug(TAG, "valid card: " + card);
            ToastDialog dialog = new ToastDialog(MainAbility.this);
            dialog.setText("Card valid and complete");
            ((Text) dialog.getComponent()).setMultipleLine(true);
            dialog.show();
        }
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        final CreditCardForm noZipForm = (CreditCardForm) findComponentById(ResourceTable.Id_form_no_zip);
        noZipForm.setOnCardValidCallback(cardValidCallback);

        // we can track gaining or losing focus for any particular field.
        noZipForm.setFocusChangedListener(new Component.FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean hasFocus) {
                LogUtil.debug(TAG, component.getClass().getSimpleName() + " " + (hasFocus ? "gained" : "lost") + " focus. card valid: " + noZipForm.isCreditCardValid());
            }
        });

        final CreditCardForm zipForm = (CreditCardForm) findComponentById(ResourceTable.Id_form_with_zip);
        zipForm.setOnCardValidCallback(cardValidCallback);
        final CreditCardForm yellowForm = (CreditCardForm) findComponentById(ResourceTable.Id_yellow_form);
        yellowForm.setOnCardValidCallback(cardValidCallback);
        final CreditCardForm justCard   = (CreditCardForm) findComponentById(ResourceTable.Id_just_card_form);
        justCard.setOnCardValidCallback(cardValidCallback);
        final CreditCardForm cardAndZip   = (CreditCardForm) findComponentById(ResourceTable.Id_card_and_zip_form);
        cardAndZip.setOnCardValidCallback(cardValidCallback);

        final CreditCardForm prepopulated = (CreditCardForm) findComponentById(ResourceTable.Id_pre_populated_form);
        prepopulated.setOnCardValidCallback(cardValidCallback);
        // populate the card, but don't try to focus the next field
        prepopulated.setCardNumber("4242 4242 4242 4242", false);

        final CreditCardForm clear = (CreditCardForm) findComponentById(ResourceTable.Id_clear_test_form);
        findComponentById(ResourceTable.Id_clear_test_button).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                clear.clearForm();
            }
        });

        scrollView = (ScrollView) findComponentById(ResourceTable.Id_scroll_view);
        KeyboardUtils.registerSoftInputChangedListener(scrollView, new KeyboardUtils.OnSoftInputChangedListener() {
            @Override
            public void onSoftInputChanged(int height) {
                if (height == 0) { // 说明对话框隐藏
                    KeyboardUtils.moveDown(scrollView);
                } else {
                    // when show keyboard, move up
                    KeyboardUtils.moveUpToKeyboard(height, scrollView);
                }
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        KeyboardUtils.removeLayoutChangeListener(scrollView);
    }
}
