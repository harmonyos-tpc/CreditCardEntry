/* * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.devmarvel.creditcardentrydemo;

import com.devmarvel.creditcardentry.library.CardValidCallback;
import com.devmarvel.creditcardentry.library.CreditCard;
import com.devmarvel.creditcardentry.library.CreditCardForm;
import com.devmarvel.creditcardentry.util.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.ToastDialog;

public class SimpleAbility extends Ability {
    private static final String TAG = "SimpleAbility";

    CardValidCallback cardValidCallback = new CardValidCallback() {
        @Override
        public void cardValid(CreditCard card) {
            LogUtil.debug(TAG, "valid card: " + card);
            ToastDialog dialog = new ToastDialog(SimpleAbility.this);
            dialog.setText("Card valid and complete");
            ((Text) dialog.getComponent()).setMultipleLine(true);
            dialog.show();
        }
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_simple);

        final CreditCardForm zipForm = (CreditCardForm) findComponentById(ResourceTable.Id_form_with_zip);
        zipForm.setOnCardValidCallback(cardValidCallback);
    }
}
