/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.devmarvel.creditcardentrydemo;

import com.devmarvel.creditcardentry.fields.CreditCardText;
import com.devmarvel.creditcardentry.fields.ExpDateText;
import com.devmarvel.creditcardentry.fields.SecurityCodeText;
import com.devmarvel.creditcardentry.fields.ZipCodeText;
import com.devmarvel.creditcardentry.internal.CreditCardEntry;
import com.devmarvel.creditcardentry.library.CardValidCallback;
import com.devmarvel.creditcardentry.library.CreditCard;
import com.devmarvel.creditcardentry.library.CreditCardForm;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CreditCardFormOhosTest {
    private CreditCardForm creditCardForm;
    private DirectionalLayout container;
    private Context mContext;

    CardValidCallback cardValidCallback = new CardValidCallback() {
        @Override
        public void cardValid(CreditCard card) {

        }
    };

    @Before
    public void setUp() {
        mContext = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    }

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.devmarvel.creditcardentrydemo", actualBundleName);
    }

    @Test
    public void testCardNumberHint() {
        container = getContainerView();
        assertEquals("1234 5678 9012 3456", ((CreditCardText) container.getComponentAt(0)).getHint());
        creditCardForm.setCreditCardTextHint("XXXX XXXX XXXX XXXX");
        assertEquals("XXXX XXXX XXXX XXXX", ((CreditCardText) container.getComponentAt(0)).getHint());
    }

    @Test
    public void testCardExpiryHint() {
        container = getContainerView();
        assertEquals("MM/YY", ((ExpDateText) container.getComponentAt(2)).getHint());
        creditCardForm.setExpDateTextHint("AA/BB");
        assertEquals("AA/BB", ((ExpDateText) container.getComponentAt(2)).getHint());
    }

    @Test
    public void testCardCVVHint() {
        container = getContainerView();
        assertEquals("CVV", ((SecurityCodeText) container.getComponentAt(3)).getHint());
        creditCardForm.setSecurityCodeTextHint("KMM");
        assertEquals("KMM", ((SecurityCodeText) container.getComponentAt(3)).getHint());
    }

    @Test
    public void testCardZipHint() {
        container = getContainerView();
        assertEquals("ZIP      ", ((ZipCodeText) container.getComponentAt(4)).getHint());
        creditCardForm.setZipCodeTextHint("PIN");
        assertEquals("PIN", ((ZipCodeText) container.getComponentAt(4)).getHint());
    }

    @Test
    public void testCardNumberText() {
        container = getContainerView();
        creditCardForm.setCardNumber("4242424242424242", true);
        assertEquals("4242 4242 4242 4242", ((CreditCardText) container.getComponentAt(0)).getText());
    }

    @Test
    public void testCardExpiryText() {
        container = getContainerView();
        creditCardForm.setCardNumber("4242424242424242", true);
        creditCardForm.setExpDate("12/22", true);
        assertEquals("12/22", ((ExpDateText) container.getComponentAt(2)).getText());
    }

    @Test
    public void testCardCVVText() {
        container = getContainerView();
        creditCardForm.setCardNumber("4242424242424242", true);
        creditCardForm.setExpDate("12/22", true);
        creditCardForm.setSecurityCode("787", true);
        assertEquals("787", ((SecurityCodeText) container.getComponentAt(3)).getText());
    }

    @Test
    public void testCardZipText() {
        container = getContainerView();
        creditCardForm.setCardNumber("4242424242424242", true);
        creditCardForm.setExpDate("12/22", true);
        creditCardForm.setSecurityCode("787", true);
        creditCardForm.setZipCode("1211222", true);
        assertEquals("12112", ((ZipCodeText) container.getComponentAt(4)).getText());
    }

    @Test
    public void testCardFull() {
        container = getContainerView();
        creditCardForm.setCardNumber("4242424242424242", true);
        creditCardForm.setExpDate("12/22", true);
        creditCardForm.setSecurityCode("787", true);
        creditCardForm.setZipCode("1211222", true);
        assertEquals("4242 4242 4242 4242", creditCardForm.getCreditCard().getCardNumber());
        assertEquals("12", creditCardForm.getCreditCard().getExpMonth().toString());
        assertEquals("22", creditCardForm.getCreditCard().getExpYear().toString());
        assertEquals("787", creditCardForm.getCreditCard().getSecurityCode());
        assertEquals("12112", creditCardForm.getCreditCard().getZipCode());
        // Test validity
        assertTrue(creditCardForm.isCreditCardValid());
    }

    @Test
    public void testCardValidOrNot() {
        container = getContainerView();
        // With default invalid details
        assertFalse(creditCardForm.isCreditCardValid());
        // With valid details
        creditCardForm.setCardNumber("4242424242424242", true);
        creditCardForm.setExpDate("12/22", true);
        creditCardForm.setSecurityCode("787", true);
        creditCardForm.setZipCode("1211222", true);
        assertTrue(creditCardForm.isCreditCardValid());
    }

    public DirectionalLayout getContainerView() {
        creditCardForm = new CreditCardForm(mContext);
        CreditCardEntry creditCardEntry = creditCardForm.getCreditCardEntry();
        assertTrue(creditCardEntry.getChildCount() > 0);
        return (DirectionalLayout) creditCardEntry.getComponentAt(0);
    }
}